import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import RaceGenerator from '@/components/RaceGenerator'
import singleView from '@/components/singleView'
// import Car from '@/components/Car'
//  import Track from '@/components/Track'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/hello',
      name: 'Hello',
      component: HelloWorld
    },
    {
      path: '/',
      name: 'RaceGenerator',
      component: RaceGenerator
    },
    {
      path: '/app',
      name: 'singleView',
      component: singleView
    }
    // {
    //   path: '/',
    //   name: 'Track',
    //   component: Track
    // },
    // {
    //   path: '/',
    //   name: 'Car',
    //   component: Car
    // }
  ]
})
