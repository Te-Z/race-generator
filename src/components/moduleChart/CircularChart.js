import d3 from 'd3'

function CircularChart () {
  this.rootElement = undefined
  this.field = [] // arc's div for update
  this.ressourcesElement = [] // hold arc's data
  this.elementId = ''
  this.size = null // auto-generated
  this.degree = 2 * Math.PI / 360 // 2π is a full circle, /360 to get the radian value of a degree
  this.options = {
    /* detail in README.md */
    name: 'node', // default value, remplaced at the generate()
    spacing: 3,
    arcThickness: 17,
    coreCircle: 67,
    objectMargin: 5,
    duration: 3000,
    animation: 'elastic',
    arcTitleDisplayed: true,
    displayArcTitleAsArc: false,
    arcTitleSize: 1,
    textAfterValue: '',
    percentTextSize: 5 / 6,
    startAngle: 0,
    endAngle: 270,
    cornerRadius: 20,
    background: true,
    backgroundOpacity: 6,
    colorTransition: true,
    setOfColors: [],
    thresholdRelease: [80, 90],
    colorsThreshold: ['#9CCC65', '#FFCA28', '#F44336']
  }

  /**
   * @description Public access
   */
  this.generate = function (json) {
    // Allow to use current scope into called functions
    var self = this
    // Overwrite default properties if needed
    setUpObjectProperties(json, self)
    // Define <svg> size in relation to the last arc's position
    self.size = defineObjectSize(this.ressourcesElement, self)
    // Necessary so the object isn't situated at the origin of the div
    const centered = 'translate(' + this.size / 2 + ',' + this.size / 2 + ')' // center the object in the div
    /**
     * @constructs OBJECT'S_CORE
     */
    // Main Object
    this.rootElement = d3.select('#' + this.elementId).append('svg')
      .attr('id', this.elementId + '-svg')
      .attr('width', this.size)
      .attr('height', this.size)
      .append('g') // hold each arc's div
      .attr('id', this.elementId + '-g')
      .attr('transform', centered)
    // Core-Circle
    this.rootElement.append('circle')
      .attr('class', 'core-circle')
      .attr('r', this.options.coreCircle)
    // Name

    this.rootElement.append('text')
      .attr('class', 'node-core') // CSS : align center & color
      .text(this.options.name)
      .attr('previous-value', this.options.name)
      .style('font-size', ((this.options.coreCircle * Math.PI) / nameLength(self)) + 'px') // fit the text inside the circle
      .attr('dy', ((this.options.coreCircle) / nameLength(self))) // vertical-align
    /**
     * @constructs ARCS
     */
    this.ressourcesElement.forEach(function (d, index) {
      // build a field for each arc
      self.field[d.index - 1] = self.rootElement.append('g')
      .attr('id', d.text)
      // selected field to work on
      const arc = self.field[d.index - 1]
      // TITLE
      if (self.options.arcTitleDisplayed) {
        if (self.options.displayArcTitleAsArc) {
          // ARC FOR TITLES  (which is hidden via CSS)
          arc
          .append('path')
          .attr('class', 'arc-titleArced') // TODO
          .attr('id', 'path-titleArc-' + self.elementId + '-' + d.index)
          .attr('d', setTitleArcOppositeToDataArc(self.options.coreCircle, self.options.arcThickness, self.options.spacing, d.index, self))
          // TITLE TEXT ITSELF
          arc
          .append('text')
          .append('textPath')
          .attr('class', 'arc-title') // Take the color from it
          .attr('xlink:href', '#path-titleArc-' + self.elementId + '-' + d.index) // link with path for arc
          .attr('startOffset', '25%')
          .text(d.text)
          .style('font-size', self.options.arcThickness + 'px') // in relation of the current arc
        } else {
          arc
          .append('text')
          .attr('class', 'arc-title')
          .attr('text-anchor', 'end')
          .attr('x', -2) // space between text & arc
          .attr('y', setRadius(self.options.coreCircle, self.options.arcThickness, self.options.spacing, d.index)) // align it with respective arc
          .style('font-size', self.options.arcThickness * self.options.arcTitleSize + 'px') // in relation of the current arc
          .text(d.text)
          // .attr('class', 'arc-title ')
          .style('fill', () => {
            // Initialize with a selected color
            if (self.options.colorTransition) {
              return self.options.colorsThreshold[0]
            } else {
              return self.options.setOfColors[index]
            }
          })
        }
      }
      // Background
      if (self.options.background) {
        arc
        .append('path')
        .attr('class', 'arc-background') // set color
        .attr('d', setBackground(self.options.coreCircle, self.options.arcThickness, self.options.spacing, d.index, self))
        .style('opacity', (1 / (d.index * self.options.backgroundOpacity)))
      }
      // Arc ItSelf
      arc
        .append('path').attr('class', d.text)
        .style('fill', () => {
          // Initialize with a selected color
          if (self.options.colorTransition) {
            return self.options.colorsThreshold[0]
          } else {
            return self.options.setOfColors[index]
          }
        })
      // Path (hold the arc for %-text position)
      arc
      .append('path')
      .attr('id', 'path-percentArc-' + self.elementId + '-' + d.index)
      .attr('class', 'percentArc')
      // %-Text
      arc
      .append('text')
      .attr('class', 'text')
      .append('textPath')
      .attr('class', 'inArcText') // CSS
      .attr('startOffset', '49%')  // align w/ end of arc
      .attr('y', setRadius(self.options.coreCircle, self.options.arcThickness, self.options.spacing, d.index)) // align it with respective arc
      .attr('xlink:href', '#path-percentArc-' + self.elementId + '-' + d.index) // link with path for arc
      .style('font-size', (self.options.percentTextSize * self.options.arcThickness) + 'px')
      .attr('value', 0)
    })
    return this.rootElement
  }
  this.update = (jsonValues) => {
    // Allow to use current scope into called functions
    var self = this
    // Set const data to it's context && Specify which arc to work on
    const data = this.ressourcesElement[selectArc(jsonValues.arc, self)]
    /**
     * As Transition use arrays of data
     * this is used to trick the framework to update one part of the object
     * instead of the object itself
     */
    var dataSet = [{index: 0, percentage: jsonValues.value}]

    var arcTransition = function () {
      // Assign data for transition
      var localField = self.field[data.index - 1].data(dataSet)
      //  get actual value
      .each(function (d) {
        d.previousValue = $(this).find('.inArcText').attr('value')
      })
      // To know on wich arc to work on
      .each(function (d) {
        d.index = data.index
      })
      // inject current scope
      .each(function (d) {
        d.self = self
      })

      /**
       * @constructs UPDATE_TITLE_COLOR
       */
      var updateTitleColor = localField.select('.arc-title').transition().duration(self.options.duration / 10)
      if (self.options.arcTitleDisplayed && self.options.colorTransition) {
        updateTitleColor.style('fill', function (d) {
          return setColor(d.percentage, self)
        })
      } else if (jsonValues.color) {
        updateTitleColor.style('fill', jsonValues.color)
      }
      /**
       * @constructs UPDATE_ARC
       */
      var updateArc = localField.select('.' + data.text).transition().duration(self.options.duration)
      .ease(self.options.animation)
      .attrTween('d', setArcTween)
      if (self.options.colorTransition) {
        updateArc.style('fill', function (d) {
          return setColor(d.percentage, self)
        })
      }
      if (jsonValues.color) {
        updateArc.style('fill', jsonValues.color)
      }

      /* TODO DEV GRADIENT */
      if (jsonValues.gradient) {
        // let pie = d3.pie()
        // .sort(null)
        // .value(function(d) { return d; })

        // var data = [1]
        // data = pie(data)

        // this.field[data.index - 1]
        // .data(dataSet)
        // .enter()

      }
      /* END DEV GRADIENT */

      /**
       * @constructs UPDATE_%-TEXT
       */
      // ARC-PERCENT
      localField.select('.percentArc').transition().duration(self.options.duration)
      .ease(self.options.animation)
      .attrTween('d', setPercentTween)

      // TEXT DISPLAYED INSIDE THE ARC
      localField.select('.text').select('.inArcText').transition().duration(self.options.duration / 2)
      .tween('text', function (d) {
        var i = d3.interpolate(d.previousValue, (jsonValues.displayedValue != undefined ? jsonValues.displayedValue : d.percentage)) //(jsonValues.displayedValue != undefined ? jsonValues.displayedValue : d.percentage)
        return function (t) {
          if (typeof jsonValues.displayedValue == 'number') {
            // TRANSITIONS OF NUMBERS
            this.textContent = Math.round(i(t)) + (jsonValues.textAfterValue != undefined ? jsonValues.textAfterValue : self.options.textAfterValue)
          } else if (typeof jsonValues.displayedValue == 'string') {
            // DISPLAY A STRING
            this.textContent = (jsonValues.textAfterValue ? jsonValues.displayedValue + jsonValues.textAfterValue : jsonValues.displayedValue)
          }
        }
      })

      // Set actual value in attribute to be use at next pass for the transition
      localField.select('.text').select('.inArcText')
      .attr('value', function (d) {
        return Math.round(d.percentage)
      })
    }

    d3.transition().duration(this.duration).each(arcTransition)
  }
  this.updateNodeName = (value) => {
    // Static
    // value = value.toString() // TODO remove
    if (typeof value == 'string') {
      // Change the value in the DOM
      this.rootElement.select('.node-core')[0][0].textContent = value
      // Resize the value to fit into the circle
      this.rootElement.select('.node-core')[0][0].style['font-size'] = ((this.options.coreCircle * Math.PI) / nameLength(this)) + 'px' // TODO  Good ?
      // TODO vertical align needed?
      // .attr('dy', ((this.options.coreCircle) / nameLength(self))) // vertical-align
    }

    // Animated
    if(typeof value == 'number') {
      var previousValue = parseInt(d3.select('.node-core').attr('previous-value'), 10)

      d3.select('.node-core')
      .transition()
      .duration(this.options.duration / 5) // Pif-O-Meter
      .tween('text', function () {
        var i = d3.interpolate(parseInt(d3.select('.node-core').attr('previous-value'), 10), value)
        return function (t) {
          // Display the intermediate value
          this.textContent = Math.round(i(t))
          // Set the previous value into the DOM
          this.attributes['previous-value'].value = Math.round(i(t))
        }
      })
    }
  }
  this.updateNodeColor = (color, opacity) => {
    // Opacity
    d3.select('.core-circle')[0][0].style['opacity'] = opacity

    // ColorTransition
    d3.select('.core-circle')
    .transition()
    .duration(this.options.duration / 10)
    .style('fill', color)
  }
  this.updateNodeTextColor = (color) => {
    d3.select('.node-core')[0][0]['style']['fill'] = color // TODO make it better
  }

  /** -Private access-
   * @constructs FUNCTIONAL
   */
  function setUpObjectProperties (json, self) {
    // Loop on each element from the Json except the options
    for (var key in json) {
      // If element is include in the module replace it w/ data from json
      if (self.hasOwnProperty(key) && (key !== 'options')) {
        self[key] = json[key]
      } else if (key === 'arc') {
        // send the array to implement ressourcesElement
        buildArrayOfArcsData(json.arc, self)
      }
    }
    // Apply the same logic for the options
    if (json.options) {
      for (var option in json.options) {
        self.options[option] = json.options[option]
      }
    }
  }
  function defineObjectSize (data, self) {
    // calculate the position of the edge of the last arc & add margin
    return (self.options.coreCircle + (self.options.arcThickness + self.options.spacing) * data.length) * 2 + self.options.objectMargin
  }
  // Implement Data into an Array, one object per Arc
  function buildArrayOfArcsData (arrayData, self) {
    /**
     * @description build an object for each arc
     */
    var count = 1
    arrayData.forEach(
      function (d) {
        self.ressourcesElement[count - 1] = { index: count, text: d, value: 0 }
        count++
      }
    )
  }
  // Select arc for the context
  function selectArc (arc, self) {
    /**
     * @description Select needed Arc
     * @return Arc's index
     */
    var thisArc = null
    self.ressourcesElement.forEach(
      function (d) {
        if (self.ressourcesElement[d.index - 1].text === arc) {
          thisArc = d.index - 1
        }
      }
    )
    return thisArc
  }

  /** -Private access-
   * @constructs TRANSITIONS
   */
  // Arc
  function setArcTween (d) {
    var i = d3.interpolateNumber(d.previousValue, d.percentage)
    return function (t) {
      d.percentage = i(t)
      return d.self.buildArc(d)
    }
  }
  // Text-position
  function setPercentTween (d) {
    var i = d3.interpolateNumber(d.previousValue, d.percentage)
    return function (t) {
      d.percentage = i(t)
      return d.self.buildArcPercent(d)
    }
  }

     /** -Private access-
   * @constructs ARCS
   */
  this.buildArc = d3.svg.arc() // Build an arc's body
  .startAngle(() => {
    return this.options.startAngle * this.degree
  })
  .endAngle(function (d) {
    return ((this.options.endAngle - this.options.startAngle) * d.percentage) / 100 * this.degree + (this.options.startAngle * this.degree)// 1st attempt
  })
  .innerRadius(function (d) {
    return (this.options.coreCircle * d.index + this.options.spacing) - (this.options.coreCircle - this.options.spacing - this.options.arcThickness) * (d.index - 1)
  })
  .outerRadius(function (d) {
    return (this.options.coreCircle * d.index + this.options.spacing) + this.options.arcThickness - (this.options.coreCircle - this.options.spacing - this.options.arcThickness) * (d.index - 1)
  })
  .cornerRadius(function (d) {
    return this.options.cornerRadius
  })

  this.buildArcPercent = d3.svg.arc() // Build a thickless arc to calculate the text position
    .startAngle(() => {
      return this.options.startAngle * this.degree
    })
    .endAngle(function (d) {
      return ((this.options.endAngle - this.options.startAngle) * d.percentage) / 100 * this.degree + (this.options.startAngle * this.degree)
    })
    .innerRadius(function (d) { return -setRadius(this.options.coreCircle, this.options.arcThickness, this.options.spacing, d.index) })
    .outerRadius(function (d) { return -setRadius(this.options.coreCircle, this.options.arcThickness, this.options.spacing, d.index) })

  /** -Private access-
   * @constructs DESIGN
   */
  function setColor (value, self) {
    // Work with 'fill' style attribute instead of CSS
    var classColor = ''
    if (value < self.options.thresholdRelease[0]) {
      classColor = self.options.colorsThreshold[0]
    } else if (value >= self.options.thresholdRelease[0] && value < self.options.thresholdRelease[1]) {
      classColor = self.options.colorsThreshold[1]
    } else if (value >= self.options.thresholdRelease[1]) {
      classColor = self.options.colorsThreshold[2]
    }
    return classColor
  }
  // Return a position relative to the object's center (calculate arc's center of thickness to position text)
  function setRadius (radius, thickness, spacing, index) {
    const inner = (radius * index + spacing) - (radius - spacing - thickness) * (index - 1)
    const outer = (radius * index + spacing) + thickness - (radius - spacing - thickness) * (index - 1)
    return -((inner + outer) / 2 - (thickness / 4))
  }
  // Optional, apply a background for each arc
  function setBackground (radius, thickness, spacing, index, self) {
    return d3.svg.arc()
      .startAngle(() => {
        return self.options.startAngle * self.degree
      })
      .endAngle(function (d) {
        return (self.options.endAngle - self.options.startAngle) * self.degree + (self.options.startAngle * self.degree)
      })
      .innerRadius(function (d) { return (radius * index + spacing) - (radius - spacing - thickness) * (index - 1) })
      .outerRadius(function (d) { return (radius * index + spacing) + thickness - (radius - spacing - thickness) * (index - 1) })
      .cornerRadius(self.options.cornerRadius) // arc's edges
  }
  // Optional, set the title on an arc opposite to the data arc
  function setTitleArcOppositeToDataArc (radius, thickness, spacing, index, self) {
    return d3.svg.arc()
      .startAngle(() => {
        return self.options.startAngle * self.degree
      })
      .endAngle(function (d) {
        return -(360 - self.options.endAngle) * self.degree
      })
      .innerRadius(function (d) { return (radius * index + spacing * 2) - (radius - spacing - thickness) * (index - 1) })
      .outerRadius(function (d) { return (radius * index) + thickness - (radius - spacing - thickness) * (index - 1) })
      .cornerRadius(self.options.cornerRadius) // arc's edges
  }
  function nameLength (self) {
    var nameLengthValue = 0

    if (typeof self.options.name == 'number') {
      nameLengthValue = self.options.name.toString().length
    } else if (typeof self.options.name == 'string') {
      nameLengthValue = self.options.name.length
    }

    return nameLengthValue
  }
  function createGradient(d) {
    var miniArcs = []
    var angleExtent = d.endAngle - d.startAngle
    var noOfArcs = angleExtent * 75 // TODO try different values

    var colour = d3.scaleSequential(d3.interpolatePlasma)
    colour.domain([0, noOfArcs])
    var miniArcAngle = angleExtent/noOfArcs

    for (var j = 0; j < noOfArcs; j++) {
      var miniArc = {}
      miniArc.startAngle = d.startAngle + (miniArcAngle * j)
      //0.01 so the colours overlap slightly, so there's no funny artefacts.
      miniArc.endAngle = miniArc.startAngle + miniArcAngle + 0.01
      //unless it goes beyond the end of the parent arc
      miniArc.endAngle = miniArc.endAngle > d.endAngle ? d.endAngle : miniArc.endAngle
      miniArcs.push(miniArc)
    }
  }
}
export default CircularChart
